Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

  Scenario:
    When I get products by type "apple"
    Then I should get response status code 200
    And I should get the first products with the title containing "apple"

  Scenario:
    When I get products by type "mango"
    Then I should get response status code 200
    And I should get the first products with the title containing "mango"

  Scenario:
    When I get products by type "car"
    Then I should get response status code 404
