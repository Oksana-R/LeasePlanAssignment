### Application code
 
These packages generally contain application code. 
If you are writing a reusable test library, you can also place reusable test components 
such as Page Objects or Tasks here.

1. Actions for different APIs should be in the corresponding package and file, for example ProductApiActions, CustomerApiActions, etc.
2. All step definitions should be in the corresponding package and file
3. All actions and step definitions should be designed in a way, so they are reusable for different scenarios and not dependent on data
4. It is recommended to have data in a separate file, for example environment property file rather then have data hardcorded in the tests.
5. If there are some pre-conditions in the tests they should be under @Given annotation.
All feature scenarios format should be defined into Given, When, Then
6. All scenarios should be logically grouped. Serenity framework just adds additional layer of abstraction: 
one BDD step can include one or more Serenity steps which are just methods with special annotation @Step.


What was refactored (to improve according to my understanding):
1. Scenarios for product search made more readable
2. Files renamed with more appropriate names
3. StepDefinitions and Actions are splitted to make code more reusable
4. Removed duplicates in Steps
5. Updated pom.xml
6. In general, the structure of the project can be optimized. For example, ProductApiActions 
and SearchStepDefinitions should go to main/java/products. I haven't done it as project structure change caused import issues 
that I haven't got enough time to resolve.
7. Added a simple pipeline in gitlab.
8. Added serenity-single-page-report and serenity-navigator-report, I still haven't updated the pipeline to show the report there, but locally it generates report.
9. Theoretically, we should leave either Maven or Gradle - no point to keep both of them.
10. The project should also contain properties files with all variables under test/resources, instead of hardcoded data in the tests.
11. Updated TestRunner.java