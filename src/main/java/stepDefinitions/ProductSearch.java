package stepDefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.StringContains.containsStringIgnoringCase;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import actions.ProductApiActions;
import net.thucydides.core.annotations.Steps;


public class ProductSearch {

    @Steps
    private ProductApiActions productApiActions;

    @When("I get products by type {string}")
    public void whenIGetProductsBySearch(String product) {
        productApiActions.getProducts(product);
    }

    @Then("I should get response status code {int}")
    public void verifyStatusCode(int expectedStatusCode){
        restAssuredThat(response -> response.assertThat().statusCode(equalTo(expectedStatusCode)));
        //as alternative with error message
        // assertThat("Wrong response status code", productApiActions.getStatusCode(), equalTo(expectedStatusCode));
    }

    @Then("I should get the first products with the title containing {string}")
    public void verifyTitleAtribute(String product) {
        //restAssuredThat(response -> response.body("title", containsStringIgnoringCase(product)));
        assertThat("Wrong products in response", productApiActions.getResponseTitle(), containsStringIgnoringCase(product));
    }

}