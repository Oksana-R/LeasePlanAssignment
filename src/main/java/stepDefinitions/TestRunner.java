package stepDefinitions;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber-reports"},
        glue = {"src/main/java/stepDefinitions"},
        features = {"src/test/java/features/"},
        monochrome = true
)
public class TestRunner {}
