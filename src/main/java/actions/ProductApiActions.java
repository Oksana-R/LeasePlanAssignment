package actions;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ProductApiActions {

    public static final String PRODUCT_SEARCH_PATH = "https://waarkoop-server.herokuapp.com/api/v1/search/test/{product}";

    @Step
    public void getProducts(String product){
        SerenityRest.given().get(PRODUCT_SEARCH_PATH, product);
    }

    @Step
    public int getStatusCode(){
        return SerenityRest.then().extract().statusCode();
    }
    @Step
    public String getResponseTitle(){
        return SerenityRest.then().extract().body().jsonPath().get("title[0]");
    }
}